import mido
import numpy as np
from collections import namedtuple
from checkpointer import checkpoint

DecodedMidi = namedtuple('DecodedMidi', ['tracks', 'duration', 'top_volume', 'name'])
Note = namedtuple('Note', ['volume', 'note', 'start_time', 'duration'])

def note_id(evt):
  return (evt.channel, evt.note)

def is_note_on(evt):
  return evt.type == 'note_on' and evt.velocity != 0

def is_note_off(evt):
  return (evt.type == 'note_off') or (evt.type == 'note_on' and evt.velocity == 0)

def note_list_from_track(track, rate):
  times = np.cumsum([evt.time for evt in track]) * rate
  values = []
  start_by_id = {}

  for evt, start_time in zip(track, times):
    if is_note_on(evt):
      start_by_id[note_id(evt)] = evt, start_time
    elif is_note_off(evt) and note_id(evt) in start_by_id:
      start_evt, start_start_time = start_by_id[note_id(evt)]
      values.append(Note(
        volume=start_evt.velocity / 127,
        note=start_evt.note - 12,  # removes octave -1
        start_time=start_start_time,
        duration=start_time - start_start_time,
      ))

  return values

@checkpoint
def decode_midi(mid_file):
  mid = mido.MidiFile(mid_file)
  name = mid_file.split('/')[-1][:-4]
  total_time = max(sum(evt.time for evt in track) for track in mid.tracks)
  rate = mid.length / total_time
  note_lists = [note_list_from_track(track, rate) for track in mid.tracks]
  note_lists = [nl for nl in note_lists if nl]
  amp_by_sample = {}

  for note_list in note_lists:
    for note in note_list:
      start = int(note.start_time * 100)
      end = int((note.start_time + note.duration) * 100)
      for i in range(start, end):
        amp_by_sample[i] = amp_by_sample.get(i, 0) + note.volume

  top_volume = max(amp_by_sample.values())

  return DecodedMidi(note_lists, mid.length, top_volume, name)
