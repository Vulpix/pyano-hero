import pyglet
import numpy as np
from window import window


def len_of_one(*objs):
  for obj in objs:
    if hasattr(obj, '__len__'):
      return len(obj)
  return 1


def rects(batch, x, y, width, height, color=[1, 1, 1]):
  length = len_of_one(x, y, width, height)
  x = np.resize(x, length)
  y = np.resize(window.height - y - height, length)
  x2 = x + width
  y2 = y + height

  vecs = np.array([
    x, y,
    x2, y,
    x2, y2,
    x, y2,
  ]).T.reshape(-1)

  color = np.int32(np.array(color) * 255)
  color = np.resize(color, (len(x), 3))
  color = np.tile(color, 4).reshape(-1)

  batch.add(
    len(vecs) // 2,
    pyglet.gl.GL_QUADS,
    None,
    ('v2f', vecs),
    ('c3B', color)
  )


def text(batch, text, x, y, font_size, color=[1, 1, 1, 1], anchor_x='left', anchor_y='baseline'):
  return
  length = 1 if isinstance(text, str) else len(text)
  text_vec = np.resize(text, length)
  x_vec = np.resize(x, length)
  y = window.height - y - font_size
  y_vec = np.resize(y, length)
  font_size_vec = np.resize(font_size, length)
  color = np.int32(np.array(color) * 255)
  color_vec = np.resize(color, (length, 4))

  for text, x, y, font_size, color in zip(text_vec, x_vec, y_vec, font_size_vec, color_vec):
    pyglet.text.Label(
      text,
      x=x,
      y=y,
      font_size=font_size,
      font_name='Avenir',
      color=color.tolist(),
      anchor_x=anchor_x,
      anchor_y=anchor_y,
      batch=batch,
    )


def path(batch, x, y, color=[1, 1, 1]):
  y = window.height - y
  x = np.hstack([x[0], np.repeat(x[1:-1], 2), x[-1]])
  y = np.hstack([y[0], np.repeat(y[1:-1], 2), y[-1]])
  vecs = np.array([x, y]).T.reshape(-1)
  color = np.int32(np.array(color) * 255)
  color = np.tile(color, len(x))
  batch.add(len(x), pyglet.gl.GL_LINES, None, ('v2f', vecs), ('c3B', color))
