import numpy as np
from pyglet.window import key
from store import store, Track
from renderer import start_renderer
from window import window
from lib import midi_files, sample_rate, note_map
from decode_midi import decode_midi
import signal_composer
from player import Player
import midi_keyboard

player = Player()
note_frequencies = np.array([note.frequency for note in note_map])
curr_song_index = midi_files.index('gay.mid')
curr_song_index = midi_files.index('smb2-character-select.mid')


def player_callback(frame_count):
  state = store.get_state()
  start_frame = state['frame_count']
  distant_frame = start_frame + sample_rate * 5
  track = state['track']
  end_frame = start_frame + frame_count

  wave = np.zeros(frame_count)
  time = np.arange(frame_count) + start_frame

  note, start_time, end_time, _, _ = track.sheet
  top_above = start_time <= start_frame
  top_below = start_time >= distant_frame
  bottom_above = end_time <= start_frame
  bottom_below = end_time >= distant_frame
  cond = ((bottom_above == False) & (bottom_below == False)) | ((top_above == False) & (top_below == False)) | (top_above & bottom_below)

  sub_sheet = track.sheet[:, cond]
  note, start_time, end_time, volume, _ = sub_sheet

  for n, start, end, vol in zip(note, start_time, end_time, volume):
    start_i = start - start_frame
    # age = max(0, -start_i)
    start_i = max(0, start_i)
    end_i = min(end_frame, end)
    t = time[start_i:end_i]
    if len(t):
      hz = note_frequencies[n] # + pitch_bend * 40
      midi_note = signal_composer.MidiNote(t, hz, duration=(end_i - start_i) / sample_rate, sample_rate=sample_rate, volume=vol / 100)
      sub_wave = signal_composer.std_wave(midi_note) / track.decoded_midi.top_volume
      wave[start_i:end_i] += sub_wave

  store.dispatch(('SET_FRAME_COUNT', end_frame))
  store.dispatch(('SET_WAVE', wave))
  store.dispatch(('SET_SUB_SHEET', sub_sheet))
  return wave


def set_track(excluded_layers=[]):
  midi_file = midi_files[curr_song_index]
  midi_path = f'../data/midi/{midi_file}'
  decoded_midi = decode_midi(midi_path)
  num_layers = len(decoded_midi.tracks)
  selected = np.isin(np.arange(num_layers), excluded_layers) == False

  if True not in selected:
    return

  sheet = np.array([
    np.int32([note.note, note.start_time * sample_rate, (note.start_time + note.duration) * sample_rate, note.volume * 100, layer])
    for note_list, layer in zip(decoded_midi.tracks, range(num_layers))
    if layer not in excluded_layers
    for note in note_list
  ]).T

  track = Track(decoded_midi, sheet, selected)

  store.dispatch(('SET_TRACK', track))
  store.dispatch(('SET_FRAME_COUNT', store.get_state()['frame_count'] - player.frame_count))
  player_callback(player.frame_count)
  player.set_callback(player_callback)


def restart_song():
  store.dispatch(('SET_FRAME_COUNT', 0))


@window.event
def on_key_press(symbol, modifiers):
  global curr_song_index
  song_index = curr_song_index - int(key.A == symbol) + int(key.D == symbol)
  song_index = max(0, min(len(midi_files) - 1, song_index))

  if song_index != curr_song_index:
    curr_song_index = song_index
    restart_song()
    set_track([])

  if 49 <= symbol <= 57:
    track_num = symbol - 49
    excluded_track_indices = store.get_state()['track'].selected_layers == False
    if track_num < len(excluded_track_indices):
      excluded_track_indices[track_num] = not excluded_track_indices[track_num]
      excluded_layers = np.where(excluded_track_indices)[0]
      set_track(excluded_layers)


set_track([])
start_renderer()
