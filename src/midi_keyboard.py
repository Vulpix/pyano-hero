import rtmidi
import numpy as np
from time import sleep
from lib import note_map
import signal_composer
from store import store
from lib import sample_rate
from player import Player

player = Player()
midi_in = rtmidi.RtMidiIn()
note_frequencies = np.array([note.frequency for note in note_map])

def handle_midi_message(m):
  if m.isNoteOn():
    note = m.getNoteNumber()
    velocity = m.getVelocity()
    store.dispatch(('NOTE_ON', (note, velocity)))
  elif m.isNoteOff():
    note = m.getNoteNumber()
    store.dispatch(('NOTE_OFF', note))


def callback(frame_count):
  state = store.get_state()
  tick = state['tick']
  note_age = state['note_age']
  note_velocities = state['note_velocities']
  active_notes = np.where(note_velocities > 0.0)[0]
  wave = np.zeros(frame_count)
  time = np.arange(frame_count) + tick

  ports = midi_in.getPortCount()
  if ports != state['midi_ports']:
    store.dispatch(('MIDI_PORTS', ports))
    if ports:
      print('connected')
      midi_in.openPort(0)
    else:
      print('disconnected')
      midi_in.closePort()

  for note in active_notes:
    hz = note_frequencies[note] # + pitch_bend * 40
    vel = note_velocities[note] / 127

    midi_note = signal_composer.MidiNote(time, hz, duration=0, sample_rate=sample_rate, volume=vel)
    sub_signal = signal_composer.square_wave(midi_note)

    # amplitude effect stuff
    effect_duration = 1.0
    effect_samples = sample_rate * effect_duration
    effect_time = np.minimum(effect_samples, np.linspace(note_age[note], note_age[note] + frame_count, len(sub_signal))) / effect_samples

    # accordion effect
    accordion = effect_time

    # bounce effect
    bounce_freq = 20
    bounce_decay = 3
    bounce_reduction = np.exp(effect_time * bounce_decay)
    bounce = np.absolute(np.sin(effect_time * np.pi * bounce_freq)) / bounce_reduction

    effect = accordion

    wave += sub_signal * effect

  buffer = wave / np.max(np.concatenate(([1], wave)))

  store.dispatch(('TICK', frame_count))

  return buffer


midi_in.setCallback(handle_midi_message)
player.set_callback(callback)

if __name__ == '__main__':
  while stream.is_active():
    sleep(1)
