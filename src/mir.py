import numpy as np
import peakutils
from lib import sample_rate

def fft(signal, trim_by=10, log_scale=False):
  buffer_size = len(signal)
  left, right = np.split(np.abs(np.fft.fft(signal)), 2)
  ys = left + right[::-1]
  if log_scale:
    ys = np.multiply(20, np.log10(ys))
  xs = np.arange(buffer_size / 2, dtype=float)
  if trim_by:
    i = int(buffer_size / 2 / trim_by)
    ys = ys[:i]
    xs = xs[:i] * sample_rate / buffer_size
  return xs, ys

def get_peaks(xs, ys):
  peaks_indexes = peakutils.indexes(ys, thres=0.2, min_dist=5)
  freqs = [round(xs[i]) for i in peaks_indexes]
  return freqs
