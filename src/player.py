import pyaudio
import numpy as np
from lib import sample_rate

p = pyaudio.PyAudio()

class Player:
  def callback(self, in_data, frame_count, time_info, status):
    buffer = np.float32(self.inner_cb(frame_count))
    return (buffer, pyaudio.paContinue)

  frame_count = 1024

  def set_callback(self, cb):
    self.inner_cb = cb

    if not hasattr(self, 'stream') or not self.stream.is_active():
      self.stream = p.open(
        format=pyaudio.paFloat32,
        channels=1,
        rate=sample_rate,
        output=True,
        stream_callback=self.callback,
        frames_per_buffer=self.frame_count,
      )
