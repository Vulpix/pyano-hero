import pyglet
import numpy as np
from checkpointer import checkpoint
from scipy.ndimage import zoom
import draw
from window import window
from store import store
from lib import sample_rate, note_map

all_track_colors = np.random.rand(100, 3)
num_total_keys = len(note_map)

@checkpoint(format='memory', path=lambda track: (np.min(track[0]), np.max(track[0])))
def get_key_vectors(sheet):
  min_note = np.int32(np.floor(np.min(sheet[0]) / 12) * 12)
  max_note = np.int32(np.ceil(np.max(sheet[0]) / 12) * 12)
  num_keys = max_note - min_note
  keys = np.arange(num_keys)
  white_indices = np.ones(num_keys).astype(bool)
  white_indices[np.isin(keys % 12, [1, 3, 6, 8, 10])] = False

  num_white = np.count_nonzero(white_indices)
  num_black = np.count_nonzero(white_indices == False)
  num_white_cumsum = np.cumsum(white_indices)
  num_white_cumsum = np.hstack([0, num_white_cumsum[:-1]])

  key_width = window.width / num_white
  black_key_width = key_width / 2
  key_margin = 1
  note_x = num_white_cumsum * key_width
  note_x[white_indices == False] -= (black_key_width - key_margin) / 2
  note_x += (window.width - np.max(note_x) - key_width) // 2
  note_width = np.resize(key_width - key_margin, num_keys)
  note_width[white_indices == False] = black_key_width - key_margin

  return min_note, num_keys, keys, white_indices, num_white, num_black, note_x, note_width


def draw_waveform(batch, wave):
  if len(wave):
    wave = zoom(wave, window.width / len(wave))
    x = np.arange(len(wave)) / len(wave) * window.width
    y = window.height / 2 + wave * 100
    draw.path(batch, x, y, color=[0, 0.3, 0.3])


def draw_keyboard(batch, key_vectors, note_velocities, invalid_notes):
  min_note, num_keys, keys, white_indices, num_white, num_black, note_x, note_width = key_vectors
  active_indices = note_velocities > 0

  x = np.hstack([note_x[white_indices], note_x[white_indices == False]])
  width = np.hstack([note_width[white_indices], note_width[white_indices == False]])

  color = np.tile([1., 1., 1.], (num_keys, 1))
  color[white_indices == False] = [0, 0, 0]
  color[active_indices] = [0.5, 0.5, 0.5]
  color[invalid_notes] = [0.5, 0, 0]
  color = np.concatenate([color[white_indices], color[white_indices == False]], axis=0)

  draw.rects(
    batch,
    x=x,
    y=window.height - 50,
    width=width,
    height=np.hstack([np.resize(50, num_white), np.resize(30, num_black)]),
    color=color,
  )


def draw_notes(batch, key_vectors, track, sheet, frame_count, note_velocities):
  min_note, num_keys, keys, white_indices, num_white, num_black, note_x, note_width = key_vectors
  total_height = 500 - 50
  notes_per_sec = 5
  distant_frame = frame_count + sample_rate * notes_per_sec

  note, start_time, end_time, _, track_num = sheet
  note = note - min_note
  duration = end_time - start_time

  x = note_x[note]
  y = total_height - (end_time - frame_count) / (distant_frame - frame_count) * total_height
  width = note_width[note]
  height = (duration / sample_rate) * (total_height / notes_per_sec)

  active_notes = keys[note_velocities > 0]
  hittable = (start_time - frame_count) / sample_rate <= 0
  hittable_notes = np.unique(note[hittable])
  hitted_notes = np.intersect1d(active_notes, hittable_notes)
  missed_notes = np.setdiff1d(hittable_notes, hitted_notes)
  invalid_notes = np.setdiff1d(active_notes, hittable_notes)
  hitted = hittable & np.isin(note, hitted_notes)
  missed = hittable & np.isin(note, missed_notes)

  color = all_track_colors[track_num]
  color[missed] = [1, 0, 0]
  color[hitted] = [0, 1, 1]
  color_multiplier = ((note % 12) + 6) / (12 * 2)
  color = color * color_multiplier.reshape(-1, 1)

  draw.text(batch, track.decoded_midi.name, x=window.width / 2, y=10, font_size=20, color=[0.15, 0.15, 0.15, 1], anchor_x='center')
  draw.rects(batch, x, y, width, height, color)

  return hitted_notes, missed_notes, invalid_notes


def draw_octave_lines(batch, key_vectors):
  min_note, note_x, note_width = key_vectors[0], key_vectors[6], key_vectors[7]
  key_width = note_width[0]
  x = note_x[::12] - 1
  start_octave = min_note // 12 + 1
  octave_str = (np.arange(len(x)) + start_octave).astype(str)
  draw.rects(batch, x[1:], 0, 1, window.height - 50, [0.15, 0.15, 0.15])
  text_x = x + key_width * 3.5
  draw.text(batch, octave_str, x=text_x, y=window.height - 90, font_size=30, color=[0.15, 0.15, 0.15, 1], anchor_x='center')


def draw_available_layers(batch, selected_layers):
  num_tracks = len(selected_layers)
  tracks = np.arange(num_tracks)
  track_str = (tracks + 1).astype(str)
  x = tracks * 15 + 10
  color = all_track_colors[tracks]
  color[selected_layers == False] = [0.2, 0.2, 0.2]
  draw.rects(batch, x, 10, 13, 13, color)
  draw.text(batch, track_str, x=x + 2, y=11, font_size=10, color=[0, 0, 0, 1])


def draw_score(batch, score):
  draw.text(batch, str(score), x=10, y=30, font_size=16, color=[1, 1, 1, 1])


@window.event
def on_draw():
  batch = pyglet.graphics.Batch()
  state = store.get_state()
  track = state['track']
  sub_sheet = state['sub_sheet']
  key_vectors = get_key_vectors(track.sheet)
  min_note, num_keys = key_vectors[0], key_vectors[1]
  note_velocities = state['note_velocities'][min_note:min_note + num_keys]

  draw_waveform(batch, state['wave'])
  draw_octave_lines(batch, key_vectors)
  hitted_notes, missed_notes, invalid_notes = draw_notes(batch, key_vectors, track, sub_sheet, state['frame_count'], note_velocities)
  draw_keyboard(batch, key_vectors, note_velocities, invalid_notes)
  draw_available_layers(batch, track.selected_layers)
  draw_score(batch, state['score'])

  window.clear()
  batch.draw()

  points = len(hitted_notes) - len(missed_notes) - len(invalid_notes)
  store.dispatch(('SET_SCORE', state['score'] + points))


def start_renderer(cb=lambda dt: None):
  #pyglet.gl.glScalef(2.0, 2.0, 2.0)
  pyglet.gl.glLineWidth(3.0)
  pyglet.clock.schedule_interval(cb, 1 / 60)
  pyglet.app.run()
