import numpy as np
from scipy import signal
import pyaudio
from lib import note_map
from decode_midi import decode_midi
from checkpointer import checkpoint
from collections import namedtuple
from decode_midi import decode_midi

MidiNote = namedtuple('MidiNote', ['time', 'hz', 'duration', 'sample_rate', 'volume'])

def play_signal(signal, sample_rate):
  p = pyaudio.PyAudio()

  stream = p.open(
    format=pyaudio.paFloat32,
    channels=1,
    rate=sample_rate,
    output=True,
  )

  stream.write(np.float32(signal), num_frames=len(signal))
  stream.stop_stream()
  stream.close()
  p.terminate()

def sine_wave(midi_note):
  return midi_note.volume * np.sin(2.0 * np.pi * midi_note.hz * midi_note.time / midi_note.sample_rate)

def castle_wave(midi_note):
  signal = sine_wave(midi_note)
  h_amp = int(midi_note.volume / 2)
  signal[signal >= h_amp] = midi_note.volume
  signal[(signal < h_amp) & (signal > -h_amp)] = 0
  signal[signal <= -h_amp] = -midi_note.volume
  return signal

def square_wave(midi_note):
  return np.sign(sine_wave(midi_note)) * midi_note.volume

def almost_saw_wave(midi_note):
  return sine_wave(midi_note).round() * volume

def epilepsy_wave(midi_note):
  return signal.sawtooth(2 * np.pi * 5 * midi_note.time) * midi_note.volume

def sawtooth_wave(midi_note):
  return signal.sawtooth(2.0 * np.pi * midi_note.hz * midi_note.time / midi_note.sample_rate) * midi_note.volume

def square_minus_sine_wave(midi_note):
  return sawtooth_wave(midi_note) - sine_wave(midi_note)

def misery_wave(midi_note):
  time = midi_note.time / midi_note.sample_rate
  return np.sin(2.0 * np.pi * np.sin(midi_note.hz * time)) * midi_note.volume

def rubber_wave(midi_note, shake_freq=0.5, shake_amp=0.001):
  time = midi_note.time / midi_note.sample_rate
  shake = 1 + np.sin(time * np.pi * 2.0 * shake_freq) * shake_amp
  return signal.sawtooth(np.sin(2.0 * np.pi * midi_note.hz * shake * time)) * midi_note.volume

def shy_modifier(signal, sample_rate):
  signal = np.array(signal)
  signal[int(sample_rate * 0.1):] = 0
  skip_length = sample_rate / 30
  time = np.arange(len(signal))
  signal[time % (skip_length * 2) < skip_length] = 0
  return signal

def thunder_modifier(signal):
  signal = np.array(signal)
  signal[::3] = 0
  return signal

def reverb_modifier(signal, sample_rate):
  delay = int(sample_rate * 0.1)
  signal += np.roll(signal, delay) * .5
  signal += np.roll(signal, delay * 2) * .25
  signal += np.roll(signal, delay * 3) * .125
  return signal

def accordion_amp(duration, sample_rate):
  samples = sample_rate * duration
  return np.arange(samples) / samples

def bounce_amp(duration, sample_rate, bounce_freq=5, bounce_decay=2):
  samples = sample_rate * duration
  bounce_reduction = np.exp(np.arange(samples) / samples * bounce_decay)
  return np.absolute(np.sin(np.arange(samples) * np.pi / samples * bounce_freq)) / bounce_reduction

def std_wave(midi_note):
  # return shy_modifier(sawtooth_wave(midi_note), sample_rate)
  # return rubber_wave(midi_note) * bounce_amp(midi_note.duration, sample_rate)
  return sawtooth_wave(midi_note)

@checkpoint
def midi_signal(midi_path, sample_rate, rate=1):
  decoded_midi = decode_midi(midi_path)
  num_layers = len(decoded_midi.tracks)
  num_samples = round(rate * decoded_midi.duration * sample_rate) + 1
  signal = np.resize(0.0, (num_layers, num_samples))

  for note_list, layer in zip(decoded_midi.tracks, range(num_layers)):
    for note in note_list:
      start_i = int(round(rate * sample_rate * note.start_time))
      hz = note_map[note.note].frequency
      duration = rate * note.duration
      time = np.arange(sample_rate * duration) + start_i
      midi_note = MidiNote(time, hz, duration, sample_rate, note.volume)
      sub_signal = std_wave(midi_note)
      # play_signal(sub_signal, sample_rate)
      end_i = start_i + len(sub_signal)
      signal[layer][start_i:end_i] += sub_signal
    signal[layer] = reverb_modifier(signal[layer], sample_rate)

  return signal

@checkpoint
def reduced_midi_signal(midi_path, selected, sample_rate, rate=1):
  signal = midi_signal(midi_path, sample_rate)
  signal = np.sum(signal[selected], axis=0)
  return signal / np.max(signal)
