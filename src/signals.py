import numpy as np
from signal_composer import std_wave, midi_signal, play_signal
from lib import note_map
from checkpointer import checkpoint

def all_notes_signal():
  return np.concatenate([std_wave(note.frequency) for note in note_map])

def all_notes_signal_alt():
  signal1 = np.concatenate([std_wave(note.frequency) for note in note_map[1:]])
  signal2 = np.concatenate([std_wave(note.frequency) for note in note_map[:-1]])
  return signal1 + signal2
