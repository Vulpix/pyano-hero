import numpy as np
from collections import namedtuple
from lib import note_map

Track = namedtuple('Track', ['decoded_midi', 'sheet', 'selected_layers'])

emptyvec = np.array([])

class Store:
  def __init__(self, reducer):
    self.reducer = reducer
    self.state = reducer(action=('_', None))

  def get_state(self):
    return self.state

  def dispatch(self, action):
    self.state = self.reducer(self.state, action)


initial_state = {
  'track': None,
  'wave': None,
  'sub_sheet': None,
  'note_velocities': np.zeros(len(note_map)),
  'note_age': np.zeros(len(note_map)),
  'midi_ports': 0,
  'tick': 0,
  'frame_count': 0,
  'score': 0,
}

def reducer(state=initial_state, action=None):
  action_type, payload = action
  if action_type == 'SET_TRACK':
    return {**state, 'track': payload}
  if action_type == 'SET_FRAME_COUNT':
    return {**state, 'frame_count': payload}
  if action_type == 'SET_WAVE':
    return {**state, 'wave': payload}
  if action_type == 'SET_SUB_SHEET':
    return {**state, 'sub_sheet': payload}
  if action_type == 'SET_SCORE':
    return {**state, 'score': payload}
  elif action_type == 'MIDI_PORTS':
    return {**state, 'midi_ports': payload}
  elif action_type == 'NOTE_ON':
    note, velocity = payload
    note_velocities = np.array(state['note_velocities'])
    note_age = np.array(state['note_age'])

    note_velocities[note] = velocity
    note_age[note] = 0

    return {
      **state,
      'note_velocities': note_velocities,
      'note_age': note_age
    }
  elif action_type == 'NOTE_OFF':
    note_velocities = np.array(state['note_velocities'])
    note_velocities[payload] = 0
    return {**state, 'note_velocities': note_velocities}
  elif action_type == 'TICK':
    note_age =  state['note_age'] + payload
    return {
      **state,
      'note_age': state['note_age'] + payload,
      'tick': state['tick'] + payload,
    }
  else:
    return state

store = Store(reducer)
